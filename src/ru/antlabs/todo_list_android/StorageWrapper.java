package ru.antlabs.todo_list_android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class StorageWrapper extends SQLiteOpenHelper
{
    public StorageWrapper(Context context)
    {
	super(context, "items.db", null, 1);
    }
    // http://www.vogella.com/articles/AndroidSQLite/article.html 
  /* public void syncAllitems(TodoItem items[])
    {
	for(TodoItem i : items)
	{
	    db.execSQL("INSERT OR REPLACE INTO Todoitems VALUES(" + i.text + "," + i.name + "," + i.date + "," + i.value + ");");
	}
    }*/

    @Override
    public void onCreate(SQLiteDatabase db)
    {
	db.execSQL("CREATE TABLE IF NOT EXIST Todoitems(text VARCHAR, name VARCHAR, date VARCHAR, value VARCHAR);");
	db.execSQL("CREATE UNIQUE INDEX name_idx ON Todoitems(text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
	Log.d(MainActivity.LOG_TAG, "onUpgrade database");
	db.execSQL("DROP TABLE IF EXISTS Todoitems");
	onCreate(db);
    }
}
