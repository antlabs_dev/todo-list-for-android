﻿package ru.antlabs.todo_list_android;

public class TodoItem
{
    @com.google.gson.annotations.SerializedName("text")
    String text;
    
    @com.google.gson.annotations.SerializedName("name")
    String name;
    
    @com.google.gson.annotations.SerializedName("date")
    String date;
    
    @com.google.gson.annotations.SerializedName("value")
    String value;

    public TodoItem(String text)
    {
	this.setText(text);
    }

    public String getText()
    {
	return this.text;
    }

    public final void setChecked(boolean value)
    {
	this.value = value ? "checked" : "unchecked";
    }
    
    public final void setText(String text)
    {
	this.text = text;
    }
    
    @Override
    public String toString()
    {
	return getText();
    }

    @Override
    public boolean equals(Object o)
    {
	return o instanceof TodoItem && ((TodoItem) o).text == this.text;
    }
}
