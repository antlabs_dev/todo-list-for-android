﻿package ru.antlabs.todo_list_android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

public class TodoItemAdapter extends ArrayAdapter<TodoItem>
{
    private Context context = null;
    private int layoutResourceId = 0;

    public TodoItemAdapter(Context context, int layoutResourceId)
    {
	super(context, layoutResourceId);
	this.context = context;
	this.layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
	View row = convertView;
	final TodoItem currentItem = getItem(position);

	if(row == null)
	{
	    LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	    row = inflater.inflate(layoutResourceId, parent, false);
	}

	row.setTag(currentItem);
	final CheckBox checkBox = (CheckBox)row.findViewById(R.id.checkBox1);
	checkBox.setText(currentItem.text);
	checkBox.setChecked(false);
	checkBox.setEnabled(true);

	checkBox.setOnClickListener(new View.OnClickListener()
	{
	    @Override
	    public void onClick(View arg0)
	    {
		if(checkBox.isChecked())
		{
		    checkBox.setEnabled(false);
		    if(context instanceof MainActivity)
		    {
			MainActivity activity = (MainActivity)context;
			activity.checkItem(currentItem);
		    }
		}
	    }
	});

	return row;
    }

}
