package ru.antlabs.todo_list_android;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends Activity
{
    public static final String LOG_TAG = "todolist";
    private TodoItemAdapter todoItemAdapter = null;

    DefaultHttpClient httpclient = new DefaultHttpClient();
    private AccountManager accountManager = null;
    private boolean expireToken = false;
    private static final String appDomain = "https://todo-done-list.appspot.com/";
    private static final String appRequesJsonTodoItems = "json?type=todoitems";
    private static final String appRequesJsonTodoItemsUpdate = "json";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);

	requestWindowFeature(Window.FEATURE_PROGRESS);
	requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

	setContentView(R.layout.activity_main);

	final ListView taskList = (ListView)findViewById(R.id.listViewTasks);
	todoItemAdapter = new TodoItemAdapter(this, R.layout.list_item);
	taskList.setAdapter(todoItemAdapter);
	
	final Button addButton = (Button)findViewById(R.id.buttonAddItem);
	addButton.setOnClickListener(new OnClickListener()
	{
	    @Override
	    public void onClick(View v)
	    {
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle(R.string.add_new_task);
		final EditText input = new EditText(MainActivity.this);
		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
		builder.setView(input);
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() 
		{ 
		    @Override
		    public void onClick(DialogInterface dialog, int which)
		    {
			addItem(input.getText().toString());
		    }
		});
		builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() 
		{
		    @Override
		    public void onClick(DialogInterface dialog, int which)
		    {
		        dialog.cancel();
		    }
		});
		builder.show();
		input.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
		    @Override
		    public void onFocusChange(View v, boolean hasFocus)
		    {
			input.post(new Runnable()
			{
			    @Override
			    public void run()
			    {
				InputMethodManager inputMethodManager = (InputMethodManager)
					MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
			    }
			});
		    }
		});
		input.requestFocus();
	    }
	});
    }
    
    @Override
    protected void onStart()
    {
	super.onStart();
	progressBarSet(false);
	startSync();
    }

    private void startSync()
    {
	if(hasInternetConnection())
	{
	    startAuthProcess();
	}
	else
	{
	    showToast(R.string.no_connection);
	}
    }
    
    public void updateTodoitems(TodoItem[] items)
    {
	//StorageWrapper s = new StorageWrapper();
	//s.syncAllitems(items);
	todoItemAdapter.clear();
	todoItemAdapter.addAll(items);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
	Intent in = null;
	switch(item.getItemId())
	{
	case R.id.action_settings:
	    in = new Intent(MainActivity.this, SettingsActivity.class);
	    in.putExtra("account", getCurrentAccount());
	    startActivity(in);
	    return true;
	case R.id.action_help:
	    in = new Intent(MainActivity.this, HelpActivity.class);
	    startActivity(in);
	    return true;
	case R.id.action_sync_now:
	    startSync();
	    return true;
	default:
	    break;
	}
	return false;
    }

    @Override
    protected void onDestroy()
    {
	super.onDestroy();
    }

    @Override
    protected void onPause()
    {
	super.onPause();
    }

    @Override
    protected void onRestart()
    {
	super.onRestart();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
	super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume()
    {
	super.onResume();
    }

    @Override
    protected void onStop()
    {
	super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
    }
    
    private boolean hasInternetConnection()
    {
	ConnectivityManager connectivity = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
	if(connectivity != null)
	{
	    NetworkInfo[] info = connectivity.getAllNetworkInfo();
	    if(info != null)
	    {
		for(int i = 0; i < info.length; i++)
		{
		    if(info[i].getState() == NetworkInfo.State.CONNECTED)
		    {
			return true;
		    }
		}
	    }
	}
	return false;
    }
    
    public void showToast(String text)
    {
	Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
    
    public void showToast(int stringResourseId)
    {
	Toast.makeText(this, getResources().getString(stringResourseId), Toast.LENGTH_SHORT).show();
    }

    public void progressBarSet(boolean visible)
    {
	setProgressBarIndeterminateVisibility(visible);
    }
    
    public void addItem(String text)
    {
	TodoItem item = new TodoItem(text);
	item.setChecked(false);
	todoItemAdapter.add(item);
	Gson gson = new Gson();
	String json = gson.toJson(item);
	new AppTodoitemsPostRequest().execute(appDomain + appRequesJsonTodoItemsUpdate, json);
    }

    public void checkItem(TodoItem item)
    {
	item.setChecked(true);
	Gson gson = new Gson();
	String json = gson.toJson(item);
	new AppTodoitemsPostRequest().execute(appDomain + appRequesJsonTodoItemsUpdate, json);
    }

    public String getCurrentAccount()
    {
	if(accountManager != null)
	{
	    return accountManager.getAccountsByType("com.google")[0].toString();
	}
	return "NULL";
    }

    public boolean startAuthProcess()
    {
	if(appDomain == null)
	{
	    return false;
	}
	progressBarSet(true);
	accountManager = AccountManager.get(getApplicationContext());
	Account[] acc = accountManager.getAccountsByType("com.google");
	accountManager.getAuthToken(acc[0], "ah", null, true, new OnTokenAcquired(), null);
	return true;
    }

    protected void setAuthToken(Bundle bundle)
    {
	String authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
	if(expireToken)
	{
	    accountManager.invalidateAuthToken("ah", authToken);
	}
	else
	{
	    Log.i(MainActivity.LOG_TAG, "Auth token: " + authToken);
	    new GetCookie().execute(authToken);
	}
    }

    private class OnTokenAcquired implements AccountManagerCallback<Bundle>
    {
	private static final int USER_PERMISSION = 989;
	
	public void run(AccountManagerFuture<Bundle> result)
	{
	    try
	    {
		Bundle bundle = (Bundle) result.getResult();
		Log.i(MainActivity.LOG_TAG, "OnTokenAcquired " + bundle.toString());
		if(bundle.containsKey(AccountManager.KEY_INTENT))
		{
		    Intent intent = bundle.getParcelable(AccountManager.KEY_INTENT);
		    intent.setFlags(intent.getFlags() & ~Intent.FLAG_ACTIVITY_NEW_TASK);
		    startActivityForResult(intent, USER_PERMISSION);
		}
		else
		{
		    setAuthToken(bundle);
		}
	    }
	    catch(Exception e)
	    {
		Log.e(LOG_TAG, "Error in " + this.getClass().getName() + ": "+ e.getMessage());
	    }
	}
    };

    private class GetCookie extends AsyncTask<String, Void, Boolean>
    {
	protected Boolean doInBackground(String... tokens)
	{
	    HttpParams params = httpclient.getParams();
	    params.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
	    try
	    {
		HttpGet httpGet = new HttpGet(appDomain + "/_ah/login?continue=http://localhost/&auth=" + tokens[0]);
		HttpResponse response = httpclient.execute(httpGet);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);
		out.close();

		if(response.getStatusLine().getStatusCode() != 302)
		{
		    Log.d(MainActivity.LOG_TAG, "No cookie : " + response.getStatusLine().getStatusCode());
		    return false;
		}

		for(Cookie cookie : httpclient.getCookieStore().getCookies())
		{
		    Log.d(MainActivity.LOG_TAG, cookie.getName());
		    if(cookie.getName().equals("ACSID") || cookie.getName().equals("SACSID"))
		    {
			Log.i(MainActivity.LOG_TAG, "Got cookie");
			return true;
		    }
		}
	    }
	    catch(Exception e)
	    {
		Log.e(LOG_TAG, "Error in " + this.getClass().getName() + ": "+ e.getMessage());
		cancel(true);
	    }
	    finally
	    {
		params.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, true);
	    }
	    return false;
	}

	protected void onPostExecute(Boolean result)
	{
	    Log.d(MainActivity.LOG_TAG, "Done cookie");
	    if(result)
	    {
		new AppTodoitemsGetRequest().execute(appDomain + appRequesJsonTodoItems);
	    }
	    else
	    {
		showToast(R.string.error_receiving_data);
		progressBarSet(false);
	    }
	}
    }

    private class AppTodoitemsGetRequest extends AsyncTask<String, Void, Boolean>
    {
	private TodoItem[] todoItems = null;
	
	protected Boolean doInBackground(String... urls)
	{
	    try
	    {
		HttpGet httpGet = new HttpGet(urls[0]);
		HttpResponse response = httpclient.execute(httpGet);
		StatusLine statusLine = response.getStatusLine();
		Log.d(MainActivity.LOG_TAG, statusLine.getReasonPhrase());
		for(Cookie cookie : httpclient.getCookieStore().getCookies())
		{
		    Log.d(MainActivity.LOG_TAG, cookie.getName());
		}
		if(statusLine.getStatusCode() == HttpStatus.SC_OK)
		{
		    ByteArrayOutputStream out = new ByteArrayOutputStream();
		    response.getEntity().writeTo(out);
		    out.close();
		    String content = out.toString();
		    Log.d(LOG_TAG, "content = " + content);
		    Gson gson = new Gson();
		    todoItems = gson.fromJson(content, TodoItem[].class);
		    return true;
		}
	    }
	    catch(Exception e)
	    {
		Log.e(LOG_TAG, "Error in " + this.getClass().getName() + ": "+ e.getMessage());
		cancel(true);
	    }
	    return false;
	}

	protected void onPostExecute(Boolean result)
	{
	    if(result)
	    {
		updateTodoitems(todoItems);
	    }
	    else
	    {
		showToast(R.string.error_receiving_data);
	    }
	    progressBarSet(false);
	}
    }
    
    private class AppTodoitemsPostRequest extends AsyncTask<String, Void, Boolean>
    {
	protected Boolean doInBackground(String... params)
	{
	    try
	    {
		// params[0] = url
		// params[1] = json
		HttpPost httpPost = new HttpPost(params[0]);
		httpPost.setEntity(new StringEntity(params[1], "UTF-8"));
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json; charset=UTF-8");
		HttpResponse response = httpclient.execute(httpPost);
		HttpEntity entity = response.getEntity();

		if(entity != null) 
		{
		    return true;
		}
	    }
	    catch(Exception e)
	    {
		Log.e(LOG_TAG, "Error in " + this.getClass().getName() + ": "+ e.getMessage());
		cancel(true);
	    }
	    return false;
	}

	protected void onPostExecute(Boolean result)
	{
	    if(!result)
	    {
		showToast(R.string.error_receiving_data);
	    }
	    progressBarSet(false);
	    startSync();
	}
    }
}
